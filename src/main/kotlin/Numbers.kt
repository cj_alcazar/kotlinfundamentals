fun main() {
    val number=8
    println(number::class.simpleName)
    val numberTwo=800000000000000000
    println(numberTwo::class.simpleName)
    val numberFour=8.12
    println(numberFour::class.simpleName)
    val numberThree=8.12F
    println(numberThree::class.simpleName)
    val parsing: Float? = number as? Float
    println(parsing)

}