fun main() {
    val numbers=listOf(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15)

    //Create a loop that will print the following based on these conditions:

    //if the value is divisible by 3 = "Ping"
    //if the value is divisible by 5 = "Pong"
    //if the value is divisible by 3 AND 5 ="PingPong"
    //else "x"

    //    numbers.forEach {
    //        if (it%3==0&&it%5==0) println("PingPong")
    //        else if (it%3==0) println("Ping")
    //        else if (it%5==0) println("Pong")
    //        else println("x")
    //    }

    //    val grades= listOf(
    //        listOf(94,82,85),
    //        listOf(83,99,97),
    //        listOf(76,89,90)
    //    )
    //1. Create a loop, that will return the highest grade of each subject.
    //    grades.forEach {
    //        println(it.maxOrNull())
    //    }

    //    grades.forEach {
    //        println(it.maxOf { result -> result })
    //    }


    // Using filter, given a list from 1-1000, display all the perfect numbers.
    //  4 = 2+1 =3
    // 5 = 1 = 1
    // 6 = 1+2+3 = 6

    val numbers2 = (1..1000).toList()
    val result= numbers2.filter {number->
        (1 until number).toList().filter { number % it == 0 }.sum() == number
    }
    println(result)


}