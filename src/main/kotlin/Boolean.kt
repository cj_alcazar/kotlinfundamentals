fun main() {
    val isNotChristmas = false
    val isChristmas= false

    println(!isChristmas&&!isChristmas)
    println(isChristmas&&isChristmas)
    println(isChristmas&&!isChristmas)

    println(!isChristmas||!isChristmas)
    println(isChristmas||isChristmas)
    println(isChristmas||!isChristmas)

    println(isChristmas.xor(true))
}