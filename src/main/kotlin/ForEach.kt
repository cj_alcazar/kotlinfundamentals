fun main() {
    val numbers=listOf(1,2,3,4,5)

    //forEach
//    numbers.forEach { number->
//        val newNumber = number*15
//        println(newNumber)
//    }
//    numbers.forEach { println(it) }
//    val newNumbers=numbers.map {it*2}
//    println(newNumbers)

    //filter-> will return a new collection, filtering the value on our condition

    val evenNumbers=numbers.filter { it%2==0 }
    println(evenNumbers)
}