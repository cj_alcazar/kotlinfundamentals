fun main() {
    //Set -> it contains unique set of elements
    //immutable
//    val usernames: Set<String> = setOf("Brandon","Amelia","Charlie")
//    println(usernames)
//    println(usernames.size)
//
//    val students:MutableSet<String> = mutableSetOf("Brandon", "Amelia","Charlie")
//    println(students)
//    students.add("Alex")
//    println(students)
//    println(students.add("Brandon") ) //Note: it is not case sensitive
//    students.add("Brandon") //If this is a list, the result would be ["Brandon", "Amelia","Charlie","Alex","Brandon"]
//    println(students)
//
//    val studentNumber:MutableList<Int> =mutableListOf(1,2,3,4,5)
//    studentNumber.add(3)
//    println(studentNumber)

    val students:MutableSet<String> = mutableSetOf("Brandon", "Amelia","Charlie")
    students.add("Alcazar") //Add a Alcazar in the set
    println(students)
    println(students.reversed()) // printing from the last to first element of the set
    println(students.size) //printing the number of elements in the set
    println(students.indices) // to print all the indices of the set
    println(students.count()) //to count the element in the set
    println(students.contains("Alcazar")) //to check if the set has a element "Alcazar"
    println(students.remove("Amelia")) //to remove the element that has "Amelia"
    println(students.elementAt(0))//to get the element of the index 0
    println(students.isEmpty()) // to check if the set is empty
    println(students.first())// to print the first value in the set
    println(students.last())//to print the last element in the set
    println(students.indexOf("Charlie")) //to print the index of the element with the same value of "Charlie"
    println(students.find { it=="Alcazars" }) // to find the element that is equals to "Alcazars" return null if now and return the "Alcazars" if true
}