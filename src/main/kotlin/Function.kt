
//fun main() {
//    //functions
//    val number = 4
//    println(isEven(number))
//
//    var number2 = 3
//    println(isEven(number2))
//}

//function to  check whether a number is odd or even
//fun isEven(num: Int): Boolean = num % 2 == 0

fun main() {

    //functions for ATMs
    //Check balance -> "Your balance is: ____"
    //Deposit -> "You just deposited ____. Your new balance is: ___"
    // Withdrawal -> "Your new balance is: ___"


    //Deposit
    // 1. You cannot deposit more than 5000
    // 2. You cannot deposit an amount not divisible by 100
    // 3. You cannot deposit negative amount

    // Withdraw
        // 1. You cannot withdraw more than half of the current amount
        // 2. You cannot withdraw negative amount
    val account = mutableMapOf<String, Any>(
        "username" to "Brandon",
        "balance" to 0
    )
    println(checkBalance(account))
    println(deposit(account,5001)) // more than 5000
    println(deposit(account,4999)) // not divisible by 100
    println(deposit(account,-133)) // negative amount
    println(withdrawal(account,3000)) //  not more than the half of balance
    println(withdrawal(account,-33)) // negative amount
    println(withdrawal(account,2000)) // accepted
    println(checkBalance(account))
}

    fun checkBalance(account: MutableMap<String, Any>): String = "Your balance is: ${account.getValue("balance")}"

    fun deposit(account: MutableMap<String, Any>, depositAmount: Int): String {
        return if (depositAmount < 0) "You cannot deposit negative amount"
        else if(depositAmount % 100 ==0) "You cannot deposit an amount not divisible by 100"
        else if(depositAmount>5000) "You cannot deposit more than 5000"
        else {
            val newBalance= account.getValue("balance").toString().toInt() + depositAmount
            account["balance"] = newBalance
            "You just deposited: $depositAmount. Your new balance is: $newBalance"
        }
    }
    fun withdrawal(account: MutableMap<String, Any>, amount: Int):String{
        return if (amount > account.getValue("balance").toString().toInt()/2) "You cannot withdraw more than half of the current amount"
        else if (amount > 0) {
            val newBalance= account.getValue("balance").toString().toInt() - amount
            account["balance"] = newBalance
            "You just withdraw: $amount. Your new balance is: $newBalance"
        } else "You cannot withdraw negative amount"

    }


