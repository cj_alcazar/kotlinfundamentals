fun main() {
    // Collections- group of related data
    // Immutable vs. Mustable
    // Immutable -> Airlines =["Cebu Pacific", "PAL", "Airasia"]
    // Mutable -> User[]
    // Topics: List, Set, Map

    //List -> ordered collections that can be accessed
    //by indices

    //immutable
//    val airlines= listOf("Cebu Pacific", "PAL", "Airasia")
//    println(airlines)
//    println(airlines[1])
//    println(airlines.get(1))
//    println(airlines.size)

    //mutable
    val terminals= mutableListOf<Int>(1,2,3,4)
    println(terminals)
    terminals.add(5)
    println(terminals)
    println(terminals.contains(2))
    print(terminals.shuffle()) // shuffling the list
    println(terminals)
    println(terminals.removeAt(1)) // Removing the element in index 1
    println(terminals)
    println(terminals.remove(3)) // Removing the element
    println(terminals)
    println(terminals.equals(3)) // to check if the list has the same value with the parameters
    println(terminals.indexOf(0)) // to get the element in index 0
    println(terminals)
    println(terminals.find { it % 2 != 0 }) // to check find the odd numbers in the list
    println(terminals.clear()) // to remove all the element in the list
    println(terminals.isEmpty()) // to check if the list is empty



}