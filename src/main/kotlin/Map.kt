fun main() {
    //Map -> key-value pairs

//    val studentDetails: Map<String,String> = mapOf(
//        "firstName" to "Christine",
//        "lastName" to "Alcazar",
//        "yrLevel" to "first",
//        "section" to "A")
//
//    println(studentDetails)
//    println(studentDetails.values)

        //mutable
    val studentDetails: MutableMap<String,String> = mutableMapOf(
        "firstName" to "Christine",
        "lastName" to "Alcazar",
        "yrLevel" to "first",
        "section" to "A")

    println(studentDetails)
    println(studentDetails.entries) //to print all the entry
    println(studentDetails.values) //to print all the value of the entry
    println(studentDetails.keys) //to print all the keys of the entry
    println(studentDetails.size) //to get the length of the Map
    println(studentDetails.get("firstName")) //to get the value of the key firstName
    println(studentDetails.remove("lastName","Alcazar")) //to remove the value with the key of lastName with equals to "Alcazar"
    println(studentDetails.getValue("section",)) //to get the value of the map with the key of Section
    println(studentDetails.isEmpty()) //to check if the map is empty or not
    println(studentDetails.count()) // to count all the element in the map
    println(studentDetails.put("lastName","Alcazar")) // to add another element in the map
    println(studentDetails)
    println(studentDetails.filterKeys { it.endsWith("e")}) // to filter the map keys that ends with the "e" suffix
    println(studentDetails.filterValues { it.endsWith("r")}) // to filter the map value that ends with the "r" suffix
    println(studentDetails.map { (e)->studentDetails.getValue(e) }) // to map the studentDetails and get the value of its key.
    // The mapping prints all the keys so it can be use as iterator of the mapping to get the values
    println(studentDetails.clear()) //to clear the map
    println(studentDetails)


}